<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as REST;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use AppBundle\Entity\Invoice;
use AppBundle\Entity\SalesInvoice;
use AppBundle\Form\InvoiceType;
use AppBundle\Form\SalesInvoiceType;

class DefaultController extends FOSRestController
{
    /**
     * @param EntityManagerInterface $em
     * @param invoiceDate
     * @return View|array
     *
     * @REST\Get("/listingInvoice/{invoiceDate}")
     * @REST\View()
     */
    public function listingInvoiceAction(EntityManagerInterface $em, $invoiceDate){
        try {
            $invoice = $em->getRepository("AppBundle:Invoice")->findByDate($invoiceDate);

            return array('invoice' => $invoice);

        } catch (\Exception $e){

            return View::create('Error: invalid date. Use format YYYY-mm', 400 );
        }
    }

    /**
     * @param EntityManagerInterface $em
     * @param invoiceDate
     * @return View|array
     *
     * @REST\Get("/listingSalesInvoice/{invoiceDate}")
     * @REST\View()
     */
    public function listingSalesInvoiceAction(EntityManagerInterface $em, $invoiceDate){

        try {
            $salesInvoice = $em->getRepository("AppBundle:SalesInvoice")->findByDate($invoiceDate);

            return array('salesInvoice' => $salesInvoice);

        } catch (\Exception $e){

            return View::create('Error: invalid date. Use format YYYY-mm', 400 );
        }
    }

    /**
     * @param Invoice $invoice
     * @return array
     * @throws NotFoundHttpException when does not exist
     *
     * @ParamConverter("invoice", class="AppBundle:Invoice")
     * @REST\Get("/getInvoice/{id}")
     * @REST\View()
     */
    public function getInvoiceAction(Invoice $invoice){

        return array('invoice' => $invoice);
    }

    /**
     * @param SalesInvoice $salesInvoice
     * @return array
     * @throws NotFoundHttpException when does not exist
     *
     * @ParamConverter("salesInvoice", class="AppBundle:SalesInvoice")
     * @REST\Get("/getSalesInvoice/{id}")
     * @REST\View()
     */
    public function getSalesInvoiceAction(SalesInvoice $salesInvoice){

        return array('salesInvoice' => $salesInvoice);
    }

    /**
     * @param Request $request
     * @param FormFactory $ff
     * @param EntityManagerInterface $em
     * @return View
     *
     * @REST\Post("/addInvoice")
     * @REST\View()
     */
    public function addInvoiceAction(Request $request, FormFactory $ff, EntityManagerInterface $em)
    {
        $form = $ff->create(InvoiceType::class, new Invoice());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            return View::create('success', 200);
        }

        return View::create($form, 400);
    }

    /**
     * @param Request $request
     * @param FormFactory $ff
     * @param EntityManagerInterface $em
     * @return View
     *
     * @REST\Post("/addSalesInvoice")
     * @REST\View()
     */
    public function addSalesInvoiceAction(Request $request, FormFactory $ff, EntityManagerInterface $em)
    {
        $form = $ff->create(SalesInvoiceType::class, new SalesInvoice());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            return View::create(null, 200);
        }

        return View::create($form, 400);
    }

    /**
     * @param Request $request
     * @param FormFactory $ff
     * @param EntityManagerInterface $em
     * @param Invoice $invoice
     * @return View
     * @throws NotFoundHttpException when does not exist
     *
     * @ParamConverter("invoice", class="AppBundle:Invoice")
     * @REST\Post("/editInvoice/{id}")
     * @REST\View()
     */
    public function editInvoiceAction(Request $request, FormFactory $ff, EntityManagerInterface $em, Invoice $invoice)
    {
        $form = $ff->create(InvoiceType::class, $invoice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            return View::create('success', 200);
        }

        return View::create($form, 400);
    }

    /**
     * @param Request $request
     * @param FormFactory $ff
     * @param EntityManagerInterface $em
     * @param SalesInvoice $salesInvoice
     * @return View
     * @throws NotFoundHttpException when does not exist
     *
     * @ParamConverter("salesInvoice", class="AppBundle:SalesInvoice")
     * @REST\Post("/editInvoice/{id}")
     * @REST\View()
     */
    public function editSalesInvoiceAction(Request $request, FormFactory $ff, EntityManagerInterface $em, SalesInvoice $salesInvoice)
    {
        $form = $ff->create(InvoiceType::class, $salesInvoice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($form->getData());
            $em->flush();

            return View::create('success', 200);
        }

        return View::create($form, 400);
    }
}